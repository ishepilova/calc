module.exports = function() {
    var temp = './.tmp/';
    
    var clientFolder = './';
    
    var config = {
        
        //All application JS files to test
        allJs: [
            './js/app/**/*.js',
            './*.js',
            '!Gruntfile.js'
        ],
        
        // Bower options
        bower: {
            json: require('./bower.json'),
            directory: './bower_components',
            ignorePath: '../..'
        },
        
        clientFolder: clientFolder,
        
        css: temp + 'styles.css',
        
        //all thml files in the app (index and partials)
        html: clientFolder + './*.html',
        
        index: 'index.html',
        
        //JS files to insert into index.html
        js: [
            './js/app/**/*.js'
        ],
        
        less: './styles/styles.less',
        
        temp: temp,
      
        server: {
          port: 9001,
          livereload: true
        }
    };
    
    config.getWiredepOptions = function () {
        var options = {
            bowerJson: config.bower.json,
            directory: config.bower.directory,
            ignorePath: config.bower.ignorePath
        };
        return options;
    };
    
    return config;
};
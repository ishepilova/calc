var gulp = require('gulp');
var args = require('yargs').argv;
var config = require('./gulp.config.js')();
var del = require('del');
var connect = require('gulp-connect');

var $ = require('gulp-load-plugins')({lazy: true});

/*
** Running JSHint and JSCS on JavaScript files
*/
gulp.task('jstest', gulp.series(function () {
    log('Running JSHint and JSCS');
    return gulp
        .src(config.allJs)
        .pipe($.if(args.verbose, $.print()))
        .pipe($.jscs())
        .pipe($.jshint())
        .pipe($.jshint.reporter('jshint-stylish', {verbose: true}))
        .pipe($.jshint.reporter('fail'));
}));

/*
** Clean CSS in temp folder
*/
gulp.task('clean-css', gulp.series(function (done) {
    var files = config.temp + '**/*.css';
    clean(files, done);
}));

/*
** Compiling Less to CSS
*/
gulp.task('less', gulp.parallel('clean-css', gulp.series(function () {
    log('Compiling Less --> CSS');
    return gulp
        .src(config.less)
        .pipe($.plumber())
        .pipe($.less())
        .pipe($.autoprefixer())
        .pipe(gulp.dest(config.temp))
        .pipe(connect.reload());
})));



/*
** Watching & Compiling LESS
*/

gulp.task('watch-less', gulp.series(function () {
    gulp.watch([config.less], gulp.series('less'));
}));

/*
** Watch JS and HTML
*/

gulp.task('html', gulp.series(function () {
  gulp.src(config.html)
    .pipe(connect.reload());
}));

gulp.task('watch-html', gulp.series(function () {
  gulp.watch([config.html], gulp.series('html'));
}));

gulp.task('watch-js', gulp.series(function () {
  gulp.watch('config.js', gulp.series('js-inject'));
}));

/*
** Connect to server
*/

gulp.task('connect', gulp.series(function () {
  connect.server({
    port: config.server.port,
    livereload: config.server.livereload
  });
}));

gulp.task('default', gulp.parallel('connect', 'watch-html', 'watch-less', 'watch-js'));

/*
** Wire up Bower dependencies
*/

gulp.task('wiredep', gulp.series(function () {
    var options = config.getWiredepOptions;
    var wiredep = require('wiredep').stream;
    
    log('Wire up Bower dependencies and app js and inject them into Html');
    
    return gulp
        .src(config.index)
        .pipe(wiredep(options))
        .pipe($.inject(gulp.src(config.js)))
        .pipe(gulp.dest(config.clientFolder));
}));

gulp.task('inject', gulp.series('wiredep', 'less', function () {
    
    log('Compiling CSS, injecting it and calling Wiredep');
    
    return gulp
        .src(config.index)
        .pipe($.inject(gulp.src(config.css)))
        .pipe(gulp.dest(config.clientFolder));
}));

gulp.task('js-inject', gulp.series('jstest', function () {
    
    log('Testing js, inserting');
    
    return gulp
        .src(config.index)
        .pipe($.inject(gulp.src(config.js)))
        .pipe(gulp.dest(config.clientFolder));
}));


/*
** Helping functions
*/

function clean(path, done) {
    log('Cleaning ' + $.util.colors.blue(path));
    del(path, done);
}

function log(msg) {
    if (typeof (msg) === 'object') {
        for (var item in msg) {
            if (msg.hasOwnProperty(item)) {
                $.util.log($.util.colors.blue(msg[item]));
            }
        }
    } else {
        $.util.log($.util.colors.blue(msg));
    }
}

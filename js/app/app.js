(function () {
    'use strict';

    angular
        .module('calc', [
            'ngRoute',
            'calc.directives',
            'calc.controllers',
            'calc.services'
        ]);
})();
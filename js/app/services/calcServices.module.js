(function() {
  'use strict';

  angular
    .module('calc.services', [
      'calc.services.values',
      'calc.services.constants',
    ]);

})();
(function () {
    'use strict';

    angular
        .module('calc.services.values', [])


        //digit key buttons
        //css classes for button on the left side
        //to remove left border (No Left Border)
        .value('numKeys', [
            {
                label: '7',
                value: 7,
                cssClass: 'nlb'
            },
            {
                label: '8',
                value: 8
            },
            {
                label: '9',
                value: 9
            },
            {
                label: '4',
                value: 4,
                cssClass: 'nlb'
            },
            {
                label: '5',
                value: 5
            },
            {
                label: '6',
                value: 6
            },
            {
                label: '1',
                value: 1,
                cssClass: 'nlb'
            },
            {
                label: '2',
                value: 2
            },
            {
                label: '3',
                value: 3
            }
        ])

        //math operations buttons
        .value('mathOperations', [
            {
                label: '÷',
                operation: function (a, b) {
                    return a / b;
                }
            },
            {
                label: '×',
                operation: function (a, b) {
                    return a * b;
                }
            },
            {
                label: '−',
                operation: function (a, b) {
                    return a - b;
                }
            },
            {
                label: '+',
                operation: function (a, b) {
                    return a + b;
                }
            }
        ]);

})();
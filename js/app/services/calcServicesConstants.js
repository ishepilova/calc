(function () {
    'use strict';

    angular
        .module('calc.services.constants', [])


    //Zero button is wider than other digit buttons and in the same row with a dot, 
    //that's why it is not with other digit buttons
    .constant('numKeys0', {
        label: '0',
        value: 0,
        cssClass: 'nbb nlb'
    })

    //'=' button
    .constant('equalSignKey', {
        label: '=',
        cssClass: 'nbb'
    });
})();
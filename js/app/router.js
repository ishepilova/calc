angular.module('calc')

.config(function ($routeProvider, $locationProvider) {

    $locationProvider.html5Mode(true);

    $routeProvider
        .when('/route', {
            templateUrl: '../partials/route.html',
            controller: 'RouteController'
        })
        .when('/', {
            templateUrl: '../partials/view.html',
            controller: 'CalcController'
        })
        .otherwise({
            redirectTo: '/'
        });

});
(function() {
  'use strict';
    
    angular
        .module('calc.controllers.main',[])
        .controller('CalcController', calcController);

    function calcController($scope,numKeys0,numKeys,equalSignKey,mathOperations){

    //Display value - value which is displayed on calc "screen"
    $scope.displayValue  = 0;
    
    //Digit buttons
    //Weird order so that our button displayed match common caclulator view
    $scope.numKeys = numKeys;
     

    $scope.mathOperations = mathOperations;

    $scope.equalSignKey = equalSignKey; 

    //stuff for calculation
    $scope.operandA      = 0;       //1st operand for calculation
    $scope.operandB      = 0;       //2nd operand for calculation
    $scope.mathOperation = null;    //operation type
    $scope.clearValue    = true;    //variable for clearing the calc display


    // Math & magic

    /** 
     * When a digit button is clicked
     * it should be appended to Display value
     */
    $scope.numClicked = function (digit) {
        if ( $scope.clearValue ) {
            $scope.displayValue = digit;
            $scope.clearValue = false;
        } else {
            $scope.displayValue = $scope.displayValue * 10 + digit;
        }
        $scope.valueB = $scope.displayValue;
    };

    /**
     * When an operation key is clicked, we save it
     * and remember 1st operand (valueA)
     * While the second operand is not set, we save 
     * 1st operand as the 2nd one (valueB).
     */
    $scope.setOperation = function ( operation ) {
        $scope.selectedOperation = operation;
        $scope.valueA = $scope.displayValue;
        $scope.valueB = $scope.displayValue;
        $scope.clearValue = true;
    };

    /**
     * Calculation and displaying the result. 
     * The result is rounded to 2 digits after decimal dot.
     * Clearing the display if a new digit button is clicked after the result displaying
     */
    $scope.calculate = function () {
        if( $scope.selectedOperation ) {
            $scope.displayValue = +(($scope.selectedOperation($scope.valueA, $scope.valueB)).toFixed(2));
            $scope.clearValue = true;
            $scope.valueA = $scope.displayValue;
        }
    };

    /**
     * Clearing everything on AC button pressing.
     */
    $scope.clearEverything = function() {
        $scope.displayValue = 0;
        $scope.valueA = 0;
        $scope.valueB = 0;
        $scope.selectedOperation = null;
    };

    $scope.toggleSign = function() {
        $scope.displayValue = -($scope.displayValue);
    };

    $scope.numKeys0 = numKeys0;
    
}

})();
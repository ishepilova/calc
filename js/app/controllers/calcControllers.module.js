(function() {
  'use strict';

  angular
    .module('calc.controllers', [
      'calc.controllers.main',
      'calc.controllers.route'
    ]);

})();
(function () {
    'use strict';

    angular
        .module('calc.controllers.route', [])
        .controller('RouteController', function ($scope) {
            var divToRemove = angular.element(document.querySelector('#RouteLinkID'));
            divToRemove.remove();
        });

})();
(function () {
    'use strict';

    angular
        .module('calc.directives.calcView', [])
        .directive('calcView', function () {
            return {
                replace: true,
                templateUrl: '../partials/view.html'
            };
        });

})();
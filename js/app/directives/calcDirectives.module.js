(function() {
  'use strict';

  angular
    .module('calc.directives', [
      'calc.directives.header',
      'calc.directives.calcView'
    ]);

})();
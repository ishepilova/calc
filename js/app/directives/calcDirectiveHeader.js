(function () {
    'use strict';

    angular
        .module('calc.directives.header', [])
        .directive('myHeader', function () {
            return {
                replace: false,
                template: '<h3>Calculator</h3>'
            };
        });

})();